package ca.shibatek.mail.service;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import ca.shibatek.exceptions.MailServiceException;
import ca.shibatek.model.MailServerAccount;
import ca.shibatek.repository.MailAccountRepository;

@ExtendWith(MockitoExtension.class)
public class MailAccountServiceTest {

	private MailAccountService fixture;
	@Mock
	private MailAccountRepository mockRepo;
	@Mock
	private MailService mockMailService;
	
	@BeforeEach
	public void setUp() {
		this.fixture = new MailAccountService(mockRepo, mockMailService);
	}
	
	@DisplayName("Should call repository.save() if MailServerAccount is valid")
	@ParameterizedTest
	@MethodSource("createValidMailServerAccounts")
	public void shouldCallRepositorySaveIfMailServerAccountValid(final MailServerAccount validAccount) throws Exception {
		Mockito.doNothing().when(mockMailService).connectToMailServer(any());		
		
		fixture.addAccount(validAccount);
		
		verify(mockRepo, times(1)).save(validAccount);

	}
	
	@DisplayName("Should not call repository.save() if validation fails")
	@ParameterizedTest
	@MethodSource("createInvalidMailServerAccounts")
	public void shouldNotCallRepositorySaveIfValidationFails(final MailServerAccount invalidAccount) throws Exception {
		MailServiceException thrown = assertThrows(MailServiceException.class, () -> fixture.addAccount(invalidAccount),
				"Expected MailFolderException to be thrown");
		
		verify(mockRepo, never()).save(invalidAccount);
		assertTrue(thrown.getMessage().contains("Account details are invalid. Please provide valid email and password"));
	}
	
	private static Stream<MailServerAccount> createValidMailServerAccounts(){
		MailServerAccount doubleDomainEmail = new MailServerAccount();
		doubleDomainEmail.setUsername("\"yura+kano\"@domain.co.jp");
		doubleDomainEmail.setPassword("password");
		
		MailServerAccount underscoreEmail =  new MailServerAccount();
		underscoreEmail.setUsername("_______@domain-one.com");
		underscoreEmail.setPassword("password");
		
		MailServerAccount hypenEmail =  new MailServerAccount();
		hypenEmail.setUsername("yura-kano@subdomain.co.com");
		hypenEmail.setPassword("password");
	
		return Stream.of(hypenEmail, underscoreEmail, doubleDomainEmail);
	}
	
	private static Stream<MailServerAccount> createInvalidMailServerAccounts(){
		MailServerAccount invalidEmail = new MailServerAccount();
		invalidEmail.setUsername("yurakano");
		invalidEmail.setPassword("password");
		
		MailServerAccount missingPassword =  new MailServerAccount();
		missingPassword.setUsername("yurakano@hotmail.com");
		missingPassword.setPassword("");
		
		MailServerAccount missingAll =  new MailServerAccount();
		missingAll.setUsername("");
		missingAll.setPassword("");
		
		return Stream.of(invalidEmail, missingPassword, missingAll);
	}
}
