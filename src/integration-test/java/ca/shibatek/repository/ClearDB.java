package ca.shibatek.repository;

import org.springframework.data.repository.CrudRepository;

import ca.shibatek.model.MailServerAccount;

public interface ClearDB extends CrudRepository<MailServerAccount, Integer> {
	

	@Override
	void deleteAll();
}

