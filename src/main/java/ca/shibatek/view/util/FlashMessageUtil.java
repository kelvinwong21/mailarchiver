package ca.shibatek.view.util;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.context.Flash;

public class FlashMessageUtil {

	private FlashMessageUtil() {
	}
	
	public static void createFlashMessage(final String message) {
		FacesContext context = FacesContext.getCurrentInstance();
		Flash flash = context.getExternalContext().getFlash();
		flash.setKeepMessages(false);
		
		context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, message, null));
	}
}
