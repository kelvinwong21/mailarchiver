package ca.shibatek.controller;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;

import org.ocpsoft.rewrite.annotation.Join;
import org.ocpsoft.rewrite.el.ELBeanName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ca.shibatek.exceptions.MailServiceException;
import ca.shibatek.mail.service.MailAccountService;
import ca.shibatek.model.MailServerAccount;
import ca.shibatek.model.MailServerType;
import ca.shibatek.view.util.FlashMessageUtil;

@Scope(value="session")
@Component(value="accountController")
@ELBeanName(value="accountController")
@Join(path="/accounts", to="/accounts.jsf")
public class AccountController {

	@Autowired
	private MailAccountService service;
	private MailServerAccount account;

	@PostConstruct
	public void init() {
		this.account = new MailServerAccount();
	}
	
	public String save() {
		try {
			service.addAccount(account);
		} catch (MailServiceException e) {
			FlashMessageUtil.createFlashMessage(e.getMessage());
		}
		this.account = new MailServerAccount();
		return "/accounts.xhtml?faces-redirect=true";
	}
	
	public MailServerAccount getAccount() {
		return account;
	}
	
	public List<MailServerAccount> getAccounts() {
		return service.getAllAccounts();
	}
	
	public List<String> getServerTypes() {
		return Stream.of(MailServerType.values())
					.map(MailServerType::name)
					.collect(Collectors.toList());
	}
}
