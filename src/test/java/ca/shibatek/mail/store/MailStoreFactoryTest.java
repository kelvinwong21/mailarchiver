package ca.shibatek.mail.store;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ca.shibatek.exceptions.MailServerConnectionException;
import ca.shibatek.model.MailServerAccount;

public class MailStoreFactoryTest {

	private MailStoreFactory fixture;
	private MailServerAccount account;
	
	@BeforeEach
	public void setUp() {
		this.fixture = new MailStoreFactory();
		this.account = new MailServerAccount();
		
		account.setMailServer("GMAIL");
		account.setUsername("user");
		account.setPassword("password");
	}
	
	@DisplayName("Should throw MailServerConnectionException when failing to connect to mail store")
	@Test
	public void shouldThrowMailServerConnectionExceptionWhenFailingToConnectStore() throws Exception {
		MailServerConnectionException thrown = assertThrows(MailServerConnectionException.class, () -> fixture.getConnectedMailStore(account),
				"Expected MailFolderException to be thrown");

		assertTrue(thrown.getMessage().contains("Cannot connect to mail store"));
	}
	
}
