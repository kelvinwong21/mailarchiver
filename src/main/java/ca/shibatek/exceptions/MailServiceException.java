package ca.shibatek.exceptions;

public class MailServiceException extends Exception{

	private static final long serialVersionUID = 1L;

	public MailServiceException(final String message) {
		super(message);
	}
}
