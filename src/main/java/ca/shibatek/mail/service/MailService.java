package ca.shibatek.mail.service;

import java.util.List;

import ca.shibatek.exceptions.MailServiceException;
import ca.shibatek.model.MailFolder;
import ca.shibatek.model.MailServerAccount;

public interface MailService {
	
	void connectToMailServer(final MailServerAccount account) throws MailServiceException;
	void closeConnection();
	boolean isConnected();
	List<MailFolder> getMailFolders() throws MailServiceException;
}
