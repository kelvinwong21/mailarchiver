package ca.shibatek.repository;

import java.util.Optional;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.StringUtils;

import ca.shibatek.application.Application;
import ca.shibatek.exceptions.MailServiceException;
import ca.shibatek.mail.service.MailAccountService;
import ca.shibatek.model.MailServerAccount;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;

@SpringBootTest(classes = {Application.class})
public class MailAccountServiceIT {

	@Autowired
	private MailAccountService fixture;
	@Autowired
	private ClearDB clearDB;

	@DisplayName("Should not add account to database if cannot connect to mailserver")
	@Test
	public void shouldNotAddAccountToDatabaseIfCannotConnectToMailServer() {
		MailServerAccount invalidAccount = getInvalidMailServerAccount();
		
		MailServiceException thrown = assertThrows(MailServiceException.class, () -> fixture.addAccount(invalidAccount),
				"Expected MailServiceException to be thrown");
		
		assertTrue(thrown.getMessage().contains("Error establishing connection to mailserver account"));
		
		Optional<MailServerAccount> account = fixture.getAccountByEmail("yura.kano@gmail.com");
		assertTrue(account.isEmpty());
	}
	
	@DisplayName("Should not add account to database if cannot connect to mailserver")
	@Test
	public void shouldNotAddAccountToDatabaseIfCredentialsAreInvalid() {
		MailServerAccount invalidAccount = new MailServerAccount();
		// password-less account should also trigger exception
		invalidAccount.setUsername("test@test.com");
		
		MailServiceException thrown = assertThrows(MailServiceException.class, () -> fixture.addAccount(invalidAccount),
				"Expected MailFolderException to be thrown");
		
		assertTrue(thrown.getMessage().contains("Account details are invalid. Please provide valid email and password"));
	
		Optional<MailServerAccount> account = fixture.getAccountByEmail("test@test.com");
		assertTrue(account.isEmpty());
	}
	
	/*
	 * Account details set from environment variable
	 */
	@DisplayName("Should add account to database if connection to mailserver succeeds")
	@Test
	public void shouldAddAccountToDatabaseIfConnectionToMailServerSucceed() throws Exception {
		final String username = System.getenv("IT_USER");
		final String password = System.getenv("IT_PWD");
		
		assertFalse(StringUtils.isEmpty(username));
		assertFalse(StringUtils.isEmpty(password));
		
		MailServerAccount validAccount = new MailServerAccount();
		validAccount.setUsername(username);
		validAccount.setPassword(password);
		validAccount.setMailServer("GMAIL");
		
		fixture.addAccount(validAccount);
		
		Optional<MailServerAccount> account = fixture.getAccountByEmail(username);
		assertFalse(account.isEmpty());	
		
		clearDB.deleteAll();
		
	}
	
	private MailServerAccount getInvalidMailServerAccount() {
		MailServerAccount invalidAccount = new MailServerAccount();
		invalidAccount.setMailServer("GMAIL");
		invalidAccount.setUsername("yura.kano@gmail.com");
		invalidAccount.setPassword("1");
		
		return invalidAccount;
	}
}
