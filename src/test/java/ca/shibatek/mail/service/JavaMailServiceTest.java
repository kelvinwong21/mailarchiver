package ca.shibatek.mail.service;

import javax.mail.Folder;
import javax.mail.MessagingException;
import javax.mail.Store;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import ca.shibatek.exceptions.MailServiceException;
import ca.shibatek.mail.service.JavaMailService;
import ca.shibatek.mail.service.MailService;
import ca.shibatek.mail.store.MailStoreFactory;
import ca.shibatek.model.MailFolder;
import ca.shibatek.model.MailServerAccount;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;


import java.util.Arrays;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class JavaMailServiceTest {
	
	private static int PLACEHOLDER_MESSAGE_COUNT = -1;
	private static String INBOX = "INBOX";
	private static String SENT = "SENT";
	
	private MailService fixture;
	@Mock
	private Store mockStore;
	@Mock 
	private Folder mockInboxFolder;
	@Mock
	private Folder mockSentFolder;
	@Mock
	private Folder mockRootFolder;
	@Mock
	private MailStoreFactory mockMailStoreFactory;
	
	@BeforeEach
	public void setup() {
		this.fixture = new JavaMailService(mockMailStoreFactory);
	}
	
	@DisplayName("Filters out mail folders that do not hold messages")
	@Test
	public void listShouldNotIncludeFolders() throws Exception {
		mockFolderBehaviour();
		mockMailStoreFactory();
		MailFolder inbox = new MailFolder();
		inbox.setName(INBOX);
		inbox.setMessageCount(0);
		
		MailFolder sent = new MailFolder();
		sent.setName(SENT);
		sent.setMessageCount(0);
				
		List<MailFolder> expected = Arrays.asList(inbox, sent);
		List<MailFolder> actual = fixture.getMailFolders();
		
		assertThat(actual, is(expected));
	}
	
	@DisplayName("List of folders should contain one placeholder when MessagingException is thrown while getting message count")
	@Test
	public void shouldHaveOnePlaceholderWhenMessagingExceptionIsThrown() throws Exception{
		mockFolderBehaviour();
		mockMailStoreFactory();
		when(mockSentFolder.getMessageCount()).thenThrow(MessagingException.class);
		
		MailFolder inbox = new MailFolder();
		inbox.setName(INBOX);
		inbox.setMessageCount(0);
		
		MailFolder sent = new MailFolder();
		sent.setName(SENT);
		sent.setMessageCount(PLACEHOLDER_MESSAGE_COUNT);
		
		List<MailFolder> expected = Arrays.asList(inbox, sent);
		List<MailFolder> actual = fixture.getMailFolders();
		
		assertThat(actual, is(expected));
	}
	
	@DisplayName("Should throw MailFolderException when handling MessagingException from get default folder")
	@Test
	public void shouldThrowMailFolderExceptionWhenGetDefaultFolder() throws Exception{
		mockMailStoreFactory();
		when(mockStore.getDefaultFolder()).thenThrow(MessagingException.class);
		assertMailFolderException();
	}
	
	@DisplayName("Should throw MailFolderException when handling MessagingException from get folder list")
	@Test
	public void shouldThrowMailFolderExceptionWhenGetFolderList() throws Exception{
		mockMailStoreFactory();
		when(mockStore.getDefaultFolder()).thenThrow(MessagingException.class);
		
		assertMailFolderException();
	}

	private void mockMailStoreFactory() throws Exception {
		MailServerAccount account = new MailServerAccount();
		when(mockMailStoreFactory.getConnectedMailStore(account)).thenReturn(mockStore);
		fixture.connectToMailServer(account);
	}
	
	@DisplayName("isConnected should return false if exception thrown while closing connection")
	@Test
	public void isConnectedShouldReturnFalseIfExceptionThrownWhileClosingConnection() throws Exception{
		mockMailStoreFactory();
		doThrow(MessagingException.class).when(mockStore).close();
		fixture.closeConnection();
		
		assertFalse(fixture.isConnected());
	}

	private void assertMailFolderException() {
		MailServiceException thrown = assertThrows(MailServiceException.class, () -> fixture.getMailFolders(),
				"Expected MailFolderException to be thrown");
		
		assertTrue(thrown.getMessage().contains("Error retrieving mail folders"));
	}
	
	private Folder[] getMockedFolderArray() {
		Folder[] folders = {mockInboxFolder, mockSentFolder, mockRootFolder};
		
		return folders;
	}
	
	private void mockFolderBehaviour() throws Exception {
		Folder[] mockedFolders = getMockedFolderArray();

		when(mockStore.getDefaultFolder()).thenReturn(mockRootFolder);
		when(mockRootFolder.list()).thenReturn(mockedFolders);
		
		when(mockInboxFolder.getFullName()).thenReturn(INBOX);
		when(mockInboxFolder.getType()).thenReturn(Folder.HOLDS_MESSAGES);
		
		when(mockSentFolder.getFullName()).thenReturn(SENT);
		when(mockSentFolder.getType()).thenReturn(Folder.HOLDS_MESSAGES);
		
		when(mockRootFolder.getType()).thenReturn(Folder.HOLDS_FOLDERS);
	}
	
	
}
