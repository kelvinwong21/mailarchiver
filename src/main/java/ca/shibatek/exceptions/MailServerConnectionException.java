package ca.shibatek.exceptions;

public class MailServerConnectionException extends Exception {

	/**
	 *  Exception thrown when there are errors connecting to the mailserver
	 */
	private static final long serialVersionUID = -1803715862292923241L;

	public MailServerConnectionException(final String message) {
		super(message);
	}
}
