package ca.shibatek.mail.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ca.shibatek.exceptions.MailServiceException;
import ca.shibatek.model.MailServerAccount;
import ca.shibatek.repository.MailAccountRepository;
import ca.shibatek.view.util.Validator;

@Component
public class MailAccountService {

	private MailAccountRepository repo;
	private MailService mailService;

	@Autowired
	public MailAccountService(MailAccountRepository repo, MailService mailService) {
		this.repo = repo;
		this.mailService = mailService;
	}
	
	public void addAccount(MailServerAccount account) throws MailServiceException {
		if (!isAccountValid(account)) {
			throw new MailServiceException("Account details are invalid. Please provide valid email and password");
		} else {
			mailService.connectToMailServer(account);
			repo.save(account);
		}
	}

	private boolean isAccountValid(final MailServerAccount account) {
		return !Validator.isValueEmpty(account.getPassword()) && Validator.isEmailValid(account.getUsername());
	}
	
	public List<MailServerAccount> getAllAccounts() {
		Iterable<MailServerAccount> iterableAccounts = repo.findAll();
		
		return StreamSupport
				.stream(iterableAccounts.spliterator(), false)
				.collect(Collectors.toList());
	}
	
	public Optional<MailServerAccount> getAccountByEmail(final String email){
		return Optional.ofNullable(repo.findByEmail(email));
	}
}
