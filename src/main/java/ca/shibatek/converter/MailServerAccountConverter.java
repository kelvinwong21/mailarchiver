package ca.shibatek.converter;

import javax.el.ValueExpression;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import ca.shibatek.bean.DownloadBean;
import ca.shibatek.model.MailServerAccount;

@FacesConverter(value="accountConverter")
public class MailServerAccountConverter implements Converter {

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String email) {
		ValueExpression valueExpression = context.getApplication().getExpressionFactory()
				.createValueExpression(context.getELContext(), "#{downloadBean}", DownloadBean.class);
		
		DownloadBean downloadBean = (DownloadBean)valueExpression.getValue(context.getELContext());
				
		return downloadBean.getSelectedAccount(email);
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object account) {
		return ((MailServerAccount)account).getUsername();
	}

}
