package ca.shibatek.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.springframework.util.StringUtils;

import ca.shibatek.model.MailServerAccount;
import lombok.Getter;
import lombok.Setter;

@ManagedBean
@SessionScoped
@Getter
@Setter
public class DownloadBean implements Serializable {

	private static final long serialVersionUID = 290699145124540164L;
	private MailServerAccount selectedAccount;
	private List<MailServerAccount> accounts;
	
	public DownloadBean() {
		this.accounts = new ArrayList<>();
		MailServerAccount account = new MailServerAccount();
		
		account.setUsername("test");
		account.setPassword("1111");
		account.setMailServer("GMAIL");
		
		
		MailServerAccount account2 = new MailServerAccount();
		
		account2.setUsername("test@3.com");
		account2.setPassword("1111");
		account2.setMailServer("GMAIL");
		
		accounts.add(account);
		accounts.add(account2);
	}
	
	public MailServerAccount getSelectedAccount(String email) {
		if(StringUtils.isEmpty(email)) {
			throw new IllegalArgumentException("Invalid email provided");
		}
		
		for(MailServerAccount account : accounts) {
			if(email.equals(account.getUsername())) {
				return account;
			}
		}
		
		return null;
	}
}
