package ca.shibatek.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ca.shibatek.model.MailServerAccount;

public interface MailAccountRepository extends CrudRepository<MailServerAccount, Integer> {
	
	@Query(value = "SELECT * FROM accounts WHERE username = ?1", nativeQuery = true)
	public MailServerAccount findByEmail(String email);
}
