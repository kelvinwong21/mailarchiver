package ca.shibatek.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Data;

@Entity
@Table(name = "Accounts", uniqueConstraints={@UniqueConstraint(columnNames= {"username"})})
@Data
public class MailServerAccount {

	@Id @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private String username;
	private String password;
	private String mailServer;
}
