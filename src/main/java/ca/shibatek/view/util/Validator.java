package ca.shibatek.view.util;

import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.util.StringUtils;

public class Validator {

	private Validator() {
		throw new AssertionError();
	}
	
	public static boolean isValueEmpty(final String value) {
		return (StringUtils.isEmpty(value));
	}
	
	public static boolean isEmailValid(final String email) {
		isValueEmpty(email);
		EmailValidator validator = EmailValidator.getInstance();
		return validator.isValid(email);
	}
	
	
}
